package com.survey.repository;


import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Category;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioCategory;

public interface ScenarioCategoryRepository extends CrudRepository<ScenarioCategory, Integer>{
	ScenarioCategory findByScenario(Scenario scenario);
	ScenarioCategory findByScenarioAndCategory(Scenario scenario,Category category);
}
