package com.survey.repository;



import org.springframework.data.repository.CrudRepository;

import com.survey.domain.UserScenario;

public interface UserScenarioRepository extends CrudRepository<UserScenario, Integer>{
	UserScenario findOne(int userScenarioId);
}
