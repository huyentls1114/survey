package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Attribute;
import com.survey.domain.ScenarioGroupa;
import com.survey.domain.ScenarioGroupaAttribute;

public interface ScenarioGroupaAttributeRepository extends CrudRepository<ScenarioGroupaAttribute, Integer>{

	ScenarioGroupaAttribute findByScenarioGroupaAndAttribute(ScenarioGroupa scenarioGroupa, Attribute attribute);

}
