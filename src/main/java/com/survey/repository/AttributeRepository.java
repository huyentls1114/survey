package com.survey.repository;

import org.springframework.data.repository.CrudRepository;


public interface AttributeRepository extends CrudRepository<com.survey.domain.Attribute, Integer> {

}
