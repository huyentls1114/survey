package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.User;

public interface UserRepository extends CrudRepository<User, Integer> {

	User findByUsername(String username);
	
}
