package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.UserScenarioQuestion;

public interface UserScenarioQuestionRepository extends CrudRepository<UserScenarioQuestion, Integer> {

}
