package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Category;
import com.survey.domain.Question;

public interface QuestionReposity extends CrudRepository<Question, Integer>{

	Question findByQuestionContentAndCategory(String questionContent, Category category);

}
