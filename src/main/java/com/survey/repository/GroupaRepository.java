package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Groupa;

public interface GroupaRepository extends CrudRepository<Groupa, Integer> {

	Groupa findByGroupaName(String groupaName);

}
