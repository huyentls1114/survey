package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Category;

public interface CategoryRepository extends CrudRepository<Category, Integer> {

}
