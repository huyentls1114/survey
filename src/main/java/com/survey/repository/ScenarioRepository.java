package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Scenario;

public interface ScenarioRepository extends CrudRepository<Scenario, Integer> {
	
}
