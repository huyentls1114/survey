package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Attribute;
import com.survey.domain.Question;
import com.survey.domain.QuestionAttribute;

public interface QuestionAttributeRepository extends CrudRepository<QuestionAttribute, Integer>{

	QuestionAttribute findByQuestionAndAttribute(Question question, Attribute attribute);

}
