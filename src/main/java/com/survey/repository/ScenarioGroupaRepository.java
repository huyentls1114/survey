package com.survey.repository;

import org.springframework.data.repository.CrudRepository;

import com.survey.domain.Groupa;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioGroupa;

public interface ScenarioGroupaRepository extends CrudRepository<ScenarioGroupa, Integer> {

	ScenarioGroupa findByScenarioAndGroupa(Scenario scenario, Groupa groupa);
	
}
