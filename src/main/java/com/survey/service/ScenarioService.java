package com.survey.service;

import com.survey.domain.Scenario;

public interface ScenarioService {

	Iterable<Scenario> findAll();


	void delete(int scenarioId);

	void save(Scenario scenario);


	Scenario findOne(int scenarioId);


	void delete(Scenario scenario2);



}
