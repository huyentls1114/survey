package com.survey.service;

import com.survey.domain.Category;

public interface CategoryService {

	Iterable<Category> findAll();

	Category findOne(int categoryId);


	void save(Category category);

	void delete(int categoryId);



}
