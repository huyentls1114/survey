package com.survey.service;

import com.survey.domain.Attribute;

public interface AttributeService {

	Iterable<Attribute> findAll();

	Attribute findOne(int attributeId);

	void delete(int attributeId);

	void save(Attribute attribute);

}
