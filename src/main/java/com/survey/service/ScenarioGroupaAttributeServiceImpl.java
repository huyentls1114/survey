package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Attribute;
import com.survey.domain.ScenarioGroupa;
import com.survey.domain.ScenarioGroupaAttribute;
import com.survey.repository.ScenarioGroupaAttributeRepository;

@Service
public class ScenarioGroupaAttributeServiceImpl implements ScenarioGroupaAttributeService {
	@Autowired
	ScenarioGroupaAttributeRepository scenarioGroupaAttributeRepository;

	@Override
	public void save(ScenarioGroupaAttribute scenarioGroupaAttribute) {
		// TODO Auto-generated method stub
		scenarioGroupaAttributeRepository.save(scenarioGroupaAttribute);
	}

	@Override
	public ScenarioGroupaAttribute findByScenarioGroupaAndAttribute(ScenarioGroupa scenarioGroupa,
			Attribute attribute) {
		// TODO Auto-generated method stub
		return scenarioGroupaAttributeRepository.findByScenarioGroupaAndAttribute(scenarioGroupa,attribute);
	}

	@Override
	public void delete(ScenarioGroupaAttribute scenarioGroupaAttribute) {
		scenarioGroupaAttributeRepository.delete(scenarioGroupaAttribute);
		
	}
	
	
}
