package com.survey.service;

import com.survey.domain.UserScenarioQuestion;

public interface UserScenarioQuestionService {
	public void save(UserScenarioQuestion userScenarioQuestion);
}
