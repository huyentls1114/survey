package com.survey.service;

import com.survey.domain.Groupa;

public interface GroupaService {

	void save(Groupa groupa);

	Groupa findOne(int groupaId);

	Groupa findByGroupaName(String groupaName);

}
