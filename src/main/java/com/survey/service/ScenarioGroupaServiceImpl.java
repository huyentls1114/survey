package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Groupa;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioGroupa;
import com.survey.repository.ScenarioGroupaRepository;

@Service
public class ScenarioGroupaServiceImpl implements ScenarioGroupaService {
	@Autowired
	ScenarioGroupaRepository scenarioGroupaRepository;
	
	@Override
	public void save(ScenarioGroupa scenarioGroupa){
		scenarioGroupaRepository.save(scenarioGroupa);
	}

	@Override
	public ScenarioGroupa findByScenarioAndGroupa(Scenario scenario, Groupa groupa) {
		// TODO Auto-generated method stub
		return scenarioGroupaRepository.findByScenarioAndGroupa(scenario,groupa);
	}

	@Override
	public void delete(ScenarioGroupa scenarioGroupa) {
		scenarioGroupaRepository.delete(scenarioGroupa);
	}
}
