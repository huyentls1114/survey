package com.survey.service;

import com.survey.domain.Category;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioCategory;

public interface ScenarioCategoryService {
	void save(ScenarioCategory scenarioCategory);

	ScenarioCategory findByScenarioAndCategory(Scenario scenario, Category category);

	void delete(ScenarioCategory scenarioCategory);

}
