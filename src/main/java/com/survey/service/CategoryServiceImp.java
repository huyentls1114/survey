package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Category;
import com.survey.repository.CategoryRepository;

@Service
public class CategoryServiceImp implements CategoryService{
	
	@Autowired
	CategoryRepository categoryRepository;

	@Override
	public Iterable<Category> findAll() {
		// TODO Auto-generated method stub
		return categoryRepository.findAll();
	}

	@Override
	public Category findOne(int categoryId) {
		// TODO Auto-generated method stub
		return categoryRepository.findOne(categoryId);
	}

	@Override
	public void save(Category category) {
		// TODO Auto-generated method stub
		categoryRepository.save(category);
		
	}

	@Override
	public void delete(int categoryId) {
		// TODO Auto-generated method stub
		categoryRepository.delete(categoryId);
	}
}
