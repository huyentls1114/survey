package com.survey.service;

import com.survey.domain.User;

public interface UserService {
	Iterable<User> findAll();
	User findOne(int userId);
	void save(User user);
	void deleteUser(int userId);
	User findByUsername(String username);
}
