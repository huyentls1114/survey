package com.survey.service;

import com.survey.domain.Groupa;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioGroupa;

public interface ScenarioGroupaService {

	void save(ScenarioGroupa scenarioGroupa);

	ScenarioGroupa findByScenarioAndGroupa(Scenario scenario, Groupa groupa);

	void delete(ScenarioGroupa scenarioGroupa);

}
