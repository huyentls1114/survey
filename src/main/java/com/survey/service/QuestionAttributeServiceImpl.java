package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Attribute;
import com.survey.domain.Question;
import com.survey.domain.QuestionAttribute;
import com.survey.domain.UserScenario;
import com.survey.domain.UserScenarioQuestion;
import com.survey.repository.QuestionAttributeRepository;
import com.survey.web.entities.vo.AttributeVO;

@Service
public class QuestionAttributeServiceImpl implements QuestionAttributeService {
	@Autowired
	QuestionAttributeRepository questionAttributeRepository;

	@Override
	public void save(QuestionAttribute questionAttribute) {
		questionAttributeRepository.save(questionAttribute);
		
	}

	@Override
	public QuestionAttribute findByQuestionAndAttribute(Question question, Attribute attribute) {
		// TODO Auto-generated method stub
		return questionAttributeRepository.findByQuestionAndAttribute(question,attribute);
	}

	@Override
	public void delete(QuestionAttribute questionAttribute) {
		questionAttributeRepository.delete(questionAttribute);
		
	}
	
	@Override
	public AttributeVO AttributeVOInSurVey(Attribute attribute,UserScenario userScenario) {
		AttributeVO attributeVO=new AttributeVO();
		attributeVO.setAttributeId(attribute.getAttributeId());
		attributeVO.setAttributeName(attribute.getAttributeName());
		int numQuestion=0;
		int numTrueAnswer = 0;
		for(UserScenarioQuestion userScenarioQuestion:userScenario.getUserScenarioQuestions()) {
			Question question=userScenarioQuestion.getQuestion();
			if(questionAttributeRepository.findByQuestionAndAttribute(question, attribute)!=null) {
				numQuestion++;
				if(userScenarioQuestion.isAnswer()==true)
					numTrueAnswer++;
			}
		}
		attributeVO.setNumQuestion(numQuestion);
		
		attributeVO.setNumTrueAnswer(numTrueAnswer);
		if(numQuestion!=0) {
		attributeVO.setScoreAttribute(numTrueAnswer*100/numQuestion);
		}
		return attributeVO;
	}
	
}
