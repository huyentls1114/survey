package com.survey.service;

import com.survey.domain.Attribute;
import com.survey.domain.Question;
import com.survey.domain.QuestionAttribute;
import com.survey.domain.UserScenario;
import com.survey.web.entities.vo.AttributeVO;

public interface QuestionAttributeService {

	void save(QuestionAttribute questionAttribute);

	QuestionAttribute findByQuestionAndAttribute(Question question, Attribute attribute);

	void delete(QuestionAttribute questionAttribute);

	AttributeVO AttributeVOInSurVey(Attribute attribute, UserScenario userScenario);

}
