package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Attribute;
import com.survey.repository.AttributeRepository;

@Service
public class AttributeServiceImp implements AttributeService {
	@Autowired
	AttributeRepository attributeRepository;

	@Override
	public Iterable<Attribute> findAll() {
		return attributeRepository.findAll();
	}

	@Override
	public Attribute findOne(int attributeId) {
		return attributeRepository.findOne(attributeId);
	}

	@Override
	public void delete(int attributeId) {
		attributeRepository.delete(attributeId);
	}

	@Override
	public void save(Attribute attribute) {
		attributeRepository.save(attribute);
	}
		
}
