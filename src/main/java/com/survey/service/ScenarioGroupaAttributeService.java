package com.survey.service;

import com.survey.domain.Attribute;
import com.survey.domain.ScenarioGroupa;
import com.survey.domain.ScenarioGroupaAttribute;

public interface ScenarioGroupaAttributeService {
	public void save(ScenarioGroupaAttribute scenarioGroupaAttribute);

	public ScenarioGroupaAttribute findByScenarioGroupaAndAttribute(ScenarioGroupa scenarioGroupa, Attribute attribute);

	public void delete(ScenarioGroupaAttribute scenarioGroupaAttribute);
}
