package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Category;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioCategory;
import com.survey.repository.ScenarioCategoryRepository;

@Service
public class ScenarioCategoryServiceImpl implements ScenarioCategoryService{
	@Autowired
	ScenarioCategoryRepository scenarioCategoryRepository;
	
	@Override
	public void save(ScenarioCategory scenarioCategory) {
		scenarioCategoryRepository.save(scenarioCategory);
		
	}

	@Override
	public ScenarioCategory findByScenarioAndCategory(Scenario scenario, Category category) {
		System.out.println(scenarioCategoryRepository);
		return scenarioCategoryRepository.findByScenarioAndCategory(scenario, category);
	}

	@Override
	public void delete(ScenarioCategory scenarioCategory) {
		scenarioCategoryRepository.delete(scenarioCategory);
		
	}


	public ScenarioCategory findByScenario(Scenario scenario){
		return scenarioCategoryRepository.findByScenario(scenario);
	}

	public Iterable<ScenarioCategory> findAllScenarioCategory(){
		return scenarioCategoryRepository.findAll();
	}
}
