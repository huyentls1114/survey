package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Scenario;
import com.survey.repository.ScenarioRepository;

@Service
public class ScenarioServiceImp implements ScenarioService {

	@Autowired
	ScenarioRepository scenarioRepository;
	
	
	
	@Override
	public Iterable<Scenario> findAll() {
		// TODO Auto-generated method stub
		return scenarioRepository.findAll();
	}

	@Override
	public void delete(int scenarioId) {
		// TODO Auto-generated method stub
		scenarioRepository.delete(scenarioId);
	}

	@Override
	public void save(Scenario scenario) {
		// TODO Auto-generated method stub
		scenarioRepository.save(scenario);
		
	}

	@Override
	public Scenario findOne(int scenarioId) {
		// TODO Auto-generated method stub
		return scenarioRepository.findOne(scenarioId);
	}
	

	@Override
	public void delete(Scenario scenario2) {
		scenarioRepository.delete(scenario2);
		
	}
}
