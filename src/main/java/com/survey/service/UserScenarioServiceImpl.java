package com.survey.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.survey.domain.UserScenario;
import com.survey.repository.UserScenarioRepository;

@Service
public class UserScenarioServiceImpl implements UserScenarioService {
	@Autowired
	UserScenarioRepository userScenarioRepository;
	
	@Override
	public void save(UserScenario userScenario) { 
		userScenarioRepository.save(userScenario);
	}
	@Override
	public UserScenario findOne(Integer userScenarioId) {
		return userScenarioRepository.findOne(userScenarioId);
	}
	
	@Override
	public List<UserScenario> findAll() {
		return (List<UserScenario>) userScenarioRepository.findAll();
	}
	@Override
	public UserScenario findOne(int userScenarioId) {
		// TODO Auto-generated method stub
		return userScenarioRepository.findOne(userScenarioId);
	}
	
	
	


}
