package com.survey.service;

import java.util.List;

import com.survey.domain.UserScenario;

public interface UserScenarioService {

	void save(UserScenario userScenario);

	UserScenario findOne(int userScenarioId);
	
	List<UserScenario> findAll();

	UserScenario findOne(Integer userScenarioId);


}
