package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Category;
import com.survey.domain.Question;
import com.survey.repository.QuestionReposity;

@Service
public class QuestionServiceIml implements QuestionService {
	
	@Autowired
	QuestionReposity questionReposity;

	@Override
	public Iterable<Question> findAll() {
		// TODO Auto-generated method stub
		return questionReposity.findAll();
	}

	@Override
	public Question findOne(int questionId) {
		// TODO Auto-generated method stub
		return questionReposity.findOne(questionId);
	}

	@Override
	public void save(Question question) {
		// TODO Auto-generated method stub
		questionReposity.save(question);
		
	}

	@Override
	public void delete(int questionId) {
		// TODO Auto-generated method stub
		questionReposity.delete(questionId);
	}

	@Override
	public Question findByQuestionContentAndCategory(String questionContent, Category category) {
		return questionReposity.findByQuestionContentAndCategory(questionContent,category);
	}
	
	
		
}
