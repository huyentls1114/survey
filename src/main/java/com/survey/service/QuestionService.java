package com.survey.service;

import com.survey.domain.Category;
import com.survey.domain.Question;

public interface QuestionService {

	Iterable<Question> findAll();

	Question findOne(int questionId);

	void save(Question question);

	void delete(int questionId);

	Question findByQuestionContentAndCategory(String questionContent, Category category);

}
