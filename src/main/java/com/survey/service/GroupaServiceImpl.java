package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.Groupa;
import com.survey.repository.GroupaRepository;
@Service
public class GroupaServiceImpl implements GroupaService {
	
	@Autowired
	GroupaRepository groupaRepository;

	@Override
	public void save(Groupa groupa) {
		// TODO Auto-generated method stub
		groupaRepository.save(groupa);
	}

	@Override
	public Groupa findOne(int groupaId) {
		// TODO Auto-generated method stub
		return groupaRepository.findOne(groupaId);
	}

	@Override
	public Groupa findByGroupaName(String groupaName) {
		// TODO Auto-generated method stub
		return groupaRepository.findByGroupaName(groupaName);
	}



}
