package com.survey.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.domain.UserScenarioQuestion;
import com.survey.repository.UserScenarioQuestionRepository;

@Service
public class UserScenarioQuestionServiceImpl implements UserScenarioQuestionService{
	@Autowired
	UserScenarioQuestionRepository userScenarioQuestionRepository;

	@Override
	public void save(UserScenarioQuestion userScenarioQuestion) {
		userScenarioQuestionRepository.save(userScenarioQuestion);
		
	}
	

}
