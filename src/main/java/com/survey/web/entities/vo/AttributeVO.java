package com.survey.web.entities.vo;


import com.survey.domain.Attribute;

public class AttributeVO{
	
	private int attributeId;
	private String attributeName;
	private GroupaVO groupaVO;
	private int score;
	private int scoreAttribute;
	private int numQuestion;
	private int numTrueAnswer;
	public int getNumQuestion() {
		return numQuestion;
	}

	public void setNumQuestion(int numQuestion) {
		this.numQuestion = numQuestion;
	}

	private boolean checked;
	public AttributeVO() {
		this.groupaVO=new GroupaVO();
	}
	
	public AttributeVO(Attribute attribute){
		this.attributeId=attribute.getAttributeId();
		this.attributeName=attribute.getAttributeName();
	}
	public int getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(int attributeId) {
		this.attributeId = attributeId;
	}
	public String getAttributeName() {
		return attributeName;
	}
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	

	public GroupaVO getGroupaVO() {
		return groupaVO;
	}

	public void setGroupaVO(GroupaVO groupaVO) {
		this.groupaVO = groupaVO;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getScoreAttribute() {
		return scoreAttribute;
	}

	public void setScoreAttribute(int scoreAttribute) {
		this.scoreAttribute = scoreAttribute;
	}

	public int getNumTrueAnswer() {
		return numTrueAnswer;
	}

	public void setNumTrueAnswer(int numTrueAnswer) {
		this.numTrueAnswer = numTrueAnswer;
	}

	
}
