package com.survey.web.entities.vo;

import java.util.ArrayList;
import java.util.List;

public class ScenarioVO {
	private int scenarioId;
	private String scenarioName;
	private List<CategoryVO> listCategoryVO =new ArrayList<>(0);
	private List<GroupaVO> listGroupaVO=new ArrayList<>();
	private List<Integer> listAttributeId=new ArrayList<>();
	private List<AttributeVO> listAttributeVO=new ArrayList<>(0);
	public int getScenarioId() {
		return scenarioId;
	}
	
	public ScenarioVO(int scenarioId, String scenarioName) {
		super();
		this.scenarioId = scenarioId;
		this.scenarioName = scenarioName;
	}

	public ScenarioVO() {
		super();
	}

	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}
	public String getScenarioName() {
		return scenarioName;
	}
	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}
	public List<CategoryVO> getListCategoryVO() {
		return listCategoryVO;
	}
	public void setListCategoryVO(List<CategoryVO> listCategoryVO) {
		this.listCategoryVO = listCategoryVO;
	}
	public List<GroupaVO> getListGroupaVO() {
		return listGroupaVO;
	}
	public void setListGroupaVO(List<GroupaVO> listGroupaVO) {
		this.listGroupaVO = listGroupaVO;
	}



	public List<Integer> getListAttributeId() {
		return listAttributeId;
	}

	public void setListAttributeId(List<Integer> listAttributeId) {
		this.listAttributeId = listAttributeId;
	}

	public List<AttributeVO> getListAttributeVO() {
		return listAttributeVO;
	}

	public void setListAttributeVO(List<AttributeVO> listAttributeVO) {
		this.listAttributeVO = listAttributeVO;
	}
	
}
