package com.survey.web.entities.vo;

import java.util.List;

public class GroupaVO {

	private int groupaId;
	private String groupaName;
	private int weight;
	private List<AttributeVO> listAttributeVO;
	private int scoreGroupa;
	public GroupaVO() {
	}
	public int getGroupaId() {
		return groupaId;
	}
	public void setGroupaId(int groupaId) {
		this.groupaId = groupaId;
	}
	public String getGroupaName() {
		return groupaName;
	}
	public void setGroupaName(String groupaName) {
		this.groupaName = groupaName;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public List<AttributeVO> getListAttributeVO() {
		return listAttributeVO;
	}
	public void setListAttributeVO(List<AttributeVO> listAttributeVO) {
		this.listAttributeVO = listAttributeVO;
	}
	
	public void addAttributeVO(AttributeVO attributeVO){
		this.listAttributeVO.add(attributeVO);
	}
	public int getScoreGroupa() {
		return scoreGroupa;
	}
	public void setScoreGroupa(int scoreGroupa) {
		this.scoreGroupa = scoreGroupa;
	}
	
	
}
