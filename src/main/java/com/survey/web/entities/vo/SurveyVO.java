package com.survey.web.entities.vo;

import java.util.List;

public class SurveyVO {
	private int userScenarioId;
	private List<QuestionVO> listQuestionVO;
	private int result;
	
	public int getUserScenarioId() {
		return userScenarioId;
	}
	public void setUserScenarioId(int userScenarioId) {
		this.userScenarioId = userScenarioId;
	}
	public List<QuestionVO> getListQuestionVO() {
		return listQuestionVO;
	}
	public void setListQuestionVO(List<QuestionVO> listQuestionVO) {
		this.listQuestionVO = listQuestionVO;
	}
	public int getResult() {
		return result;
	}
	public void setResult(int result) {
		this.result = result;
	}
	
	

}
