package com.survey.web.entities.vo;

import java.util.ArrayList;
import java.util.List;

public class ResultVO {
	private int userScenarioId;
	private int totalScore;
	private List<GroupaVO> listGroupaVO=new ArrayList<>();
	
	public int getTotalScore() {
		return totalScore;
	}
	public void setTotalScore(int totalScore) {
		this.totalScore = totalScore;
	}
	public List<GroupaVO> getListGroupaVO() {
		return listGroupaVO;
	}
	public void setListGroupaVO(List<GroupaVO> listGroupaVO) {
		this.listGroupaVO = listGroupaVO;
	}
	public int getUserScenarioId() {
		return userScenarioId;
	}
	public void setUserScenarioId(int userScenarioId) {
		this.userScenarioId = userScenarioId;
	}
	
	
}
