package com.survey.web.entities.vo;



public class CategoryVO {
	private int categoryId;
	private String categoryName;
	private int numQuestion;
	private boolean checked;
	private int categoryScore;

	public CategoryVO() {
	};

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	

	public int getNumQuestion() {
		return numQuestion;
	}

	public void setNumQuestion(int numQuestion) {
		this.numQuestion = numQuestion;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public int getCategoryScore() {
		return categoryScore;
	}

	public void setCategoryScore(int categoryScore) {
		this.categoryScore = categoryScore;
	}

}
