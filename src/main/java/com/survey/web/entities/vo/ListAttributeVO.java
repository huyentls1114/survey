package com.survey.web.entities.vo;

import java.util.List;

import com.survey.domain.Attribute;

public class ListAttributeVO {
	private List<Attribute> listAttribute;

	public List<Attribute> getListAttribute() {
		return listAttribute;
	}

	public void setListAttribute(List<Attribute> listAttribute) {
		this.listAttribute = listAttribute;
	}


}
