package com.survey.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.Attribute;
import com.survey.service.AttributeService;

@Controller
public class AttributeController {
	@Autowired
	public AttributeService attributeService;

	@RequestMapping(value ="/admin/listAttribute", method = RequestMethod.GET)
	String listAttribute(Model model) {
		model.addAttribute("attributes", attributeService.findAll());
		return "ListAttribute";
	}

	@RequestMapping("/admin/editAttribute")
	ModelAndView editCatgory(HttpServletRequest request) {
		int attributeId = Integer.parseInt(request.getParameter("attributeId"));
		Attribute attribute = attributeService.findOne(attributeId);
		ModelAndView modelAndView = new ModelAndView("AttributeForm");
		modelAndView.addObject("attribute", attribute);
		return modelAndView;
	}

	@RequestMapping(value = "/admin/deleteAttribute", method = RequestMethod.GET)
	ModelAndView deleteAttribute(HttpServletRequest request) {
		int attributeId = Integer.parseInt(request.getParameter("attributeId"));
		attributeService.delete(attributeId);
		return new ModelAndView("redirect:/admin/listAttribute");
	}

	@RequestMapping(value = "/admin/addAttribute", method = RequestMethod.GET)
	ModelAndView addAttribute(ModelAndView model) {
		Attribute attribute = new Attribute();
		model.addObject(attribute);
		model.setViewName("AttributeForm");
		return model;
	}

	@RequestMapping(value = "/admin/saveAttribute", method = RequestMethod.POST)
	public ModelAndView saveAttribute(@ModelAttribute Attribute attribute) {
		attributeService.save(attribute);
		return new ModelAndView("redirect:/admin/listAttribute");
	}
}
