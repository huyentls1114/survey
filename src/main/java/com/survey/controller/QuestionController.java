package com.survey.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.Attribute;
import com.survey.domain.Category;
import com.survey.domain.Question;
import com.survey.domain.QuestionAttribute;
import com.survey.service.AttributeService;
import com.survey.service.CategoryService;
import com.survey.service.QuestionAttributeService;
import com.survey.service.QuestionService;
import com.survey.web.entities.vo.ListAttributeVO;

@Controller
public class QuestionController {
	@Autowired
	public QuestionService questionService;
	@Autowired
	public CategoryService categoryService1;
	@Autowired
	public AttributeService attributeService;
	@Autowired
	public QuestionAttributeService questionAttributeService;
	
	@ModelAttribute("allCategories")
	public List<Category> populateVarieties() {
	    return (List<Category>) this.categoryService1.findAll();
	}
	
	@ModelAttribute("allAttributes")
	public List<Attribute> allAttributes() {
	    return (List<Attribute>) this.attributeService.findAll();
	}
	
	
	@RequestMapping(value="/admin/listQuestion",method=RequestMethod.GET)
	String listQuestion(Model model){
		model.addAttribute("questions", questionService.findAll());
		return "listQuestion";
	}
	
	@RequestMapping(value="/admin/editQuestion",method=RequestMethod.GET)
	ModelAndView editQuestion(HttpServletRequest request,Model model  ){
		int questionId= Integer.parseInt(request.getParameter("questionId"));
		Question question=questionService.findOne(questionId);
		//model.addAttribute("category", categoryService.findAll());
		ListAttributeVO listAttributeVO=new ListAttributeVO();
		listAttributeVO.setListAttribute(new ArrayList<>());
		for(QuestionAttribute questionAttribute:question.getQuestionAttributes()) {
			Attribute attribute=questionAttribute.getAttribute();
			listAttributeVO.getListAttribute().add(attribute);
		}
		
		ModelAndView modelAndView=new ModelAndView("QuestionForm");
		modelAndView.addObject(question);
		modelAndView.addObject("listAttributeVO",listAttributeVO);
		return modelAndView;
	}
	
	@RequestMapping(value="/admin/saveQuestion",method=RequestMethod.POST)
	public ModelAndView saveQuestion(@ModelAttribute Question question,@ModelAttribute ListAttributeVO listAttributeVO){
		if(question.getQuestionId()!=0) {
			Question question1=questionService.findOne(question.getQuestionId());
			for(QuestionAttribute questionAttribute:question1.getQuestionAttributes()) {
				questionAttributeService.delete(questionAttribute);
			}
		}
		questionService.save(question);
		question=questionService.findByQuestionContentAndCategory(question.getQuestionContent(),question.getCategory());
		
		for(Attribute attribute:listAttributeVO.getListAttribute()){
			QuestionAttribute questionAttribute=questionAttributeService.findByQuestionAndAttribute(question,attribute);
			if(questionAttribute==null) {
				questionAttribute=new QuestionAttribute();
				questionAttribute.setAttribute(attribute);
				questionAttribute.setQuestion(question);
			}
			questionAttributeService.save(questionAttribute);
		}
		return new ModelAndView("redirect:/admin/listQuestion");
	}
	@RequestMapping(value="/admin/deleteQuestion",method=RequestMethod.GET)
	ModelAndView deleteQuestion(HttpServletRequest request){
		int questionId=Integer.parseInt(request.getParameter("questionId"));
		questionService.delete(questionId);
		return new ModelAndView("redirect:/admin/listQuestion");
	}
	@RequestMapping(value="/admin/addQuestion",method=RequestMethod.GET)
	ModelAndView addQuestion(){
		Question question=new Question();
		ListAttributeVO listAttributeVO=new ListAttributeVO();
		ModelAndView model=new ModelAndView("QuestionForm");
		model.addObject("question",question	);
		model.addObject("listAttributeVO",listAttributeVO);
		return model;
	}
}
