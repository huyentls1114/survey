package com.survey.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.User;
import com.survey.domain.UserScenario;
import com.survey.service.UserScenarioService;
import com.survey.service.UserService;

@Controller
public class LoginController {
	@Autowired
	UserService userService;
	
	@Autowired
	UserScenarioService userScenarioService;
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout) {
		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (logout != null) {
			model.addObject("msg", "You've been logged out successfully.");
		}
		model.setViewName("login");

		return model;

	}
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request){
			String username=request.getRemoteUser();
			User userLogin=userService.findByUsername(username);
			ModelAndView model=new ModelAndView("home");
			model.addObject(userLogin);
			return model;
		}
	@GetMapping(value="/chart")
	public ModelAndView chart(){
		return new ModelAndView("chart");
	}
	
	@GetMapping(value="/example")
	public ModelAndView example(){
		return new ModelAndView("index");
	}
	
	@RequestMapping(value="/listResult",method=RequestMethod.GET)
	String listResult(Model model,HttpServletRequest request){
		String userName=request.getRemoteUser();
		User user=userService.findByUsername(userName);
		model.addAttribute("listUserScenario",user.getUserScenarios());
		return "ListResult";
	}
	
}
