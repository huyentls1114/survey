package com.survey.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.User;
import com.survey.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/admin/listUser",method=RequestMethod.GET)
	String listUser(Model model){
		model.addAttribute("users",userService.findAll());
		return "listUser";
	}
	
	@RequestMapping(value="/admin/editUser", method=RequestMethod.GET)
	ModelAndView editUser(HttpServletRequest request){
		int userId=Integer.parseInt(request.getParameter("userId"));
		User user=userService.findOne(userId);
		ModelAndView model=new ModelAndView("UserForm");
		model.addObject("user",user);
		return model;
	}
	
	@RequestMapping(value="/admin/saveUser",method=RequestMethod.POST)
	public ModelAndView saveUser(@ModelAttribute User user){
		userService.save(user);
		return new ModelAndView("redirect:/admin/listUser");
	}
	
	@RequestMapping(value="/admin/deleteUser",method=RequestMethod.GET)
	ModelAndView deleteUser(HttpServletRequest request){
		int userId=Integer.parseInt(request.getParameter("userId"));
		userService.deleteUser(userId);
		return new ModelAndView("redirect:/admin/listUser");
	}
	
	@RequestMapping(value="/admin/addUser",method=RequestMethod.GET)
	ModelAndView addUser(ModelAndView model){
		User user=new User();
		model.addObject(user);
		model.setViewName("UserForm");
		return model;
		
	}
}
