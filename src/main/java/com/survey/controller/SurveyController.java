package com.survey.controller;



import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.Attribute;
import com.survey.domain.Category;
import com.survey.domain.Question;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioCategory;
import com.survey.domain.ScenarioGroupa;
import com.survey.domain.ScenarioGroupaAttribute;
import com.survey.domain.User;
import com.survey.domain.UserScenario;
import com.survey.domain.UserScenarioQuestion;
import com.survey.service.AttributeService;
import com.survey.service.QuestionAttributeService;
import com.survey.service.QuestionService;
import com.survey.service.ScenarioService;
import com.survey.service.UserScenarioQuestionService;
import com.survey.service.UserScenarioService;
import com.survey.service.UserService;
import com.survey.web.entities.vo.AttributeVO;
import com.survey.web.entities.vo.GroupaVO;
import com.survey.web.entities.vo.QuestionVO;
import com.survey.web.entities.vo.ResultVO;
import com.survey.web.entities.vo.SurveyVO;

@Controller
public class SurveyController {
	
	@Autowired
	QuestionService questionService;
	
	@Autowired
	ScenarioService scenarioService;
	
	@Autowired
	UserScenarioService userScenarioService;
	
	@Autowired
	UserService userService;
	
	@Autowired
	UserScenarioQuestionService userScenarioQuestionService;
	
	@Autowired
	AttributeService attributeService;
	
	@Autowired
	QuestionAttributeService questionAttributeService;
	
	
	@ModelAttribute("allScenarios")
	public List<Scenario> allScenarios() {
	    return (List<Scenario>) this.scenarioService.findAll();
	}
	
	
	
	@RequestMapping(value="/selectScenario",method=RequestMethod.GET)
	ModelAndView selectScenario(HttpServletRequest request){
		User user=userService.findByUsername(request.getRemoteUser());
		UserScenario userScenario=new UserScenario();
		userScenario.setUser(user);
		ModelAndView model=new ModelAndView("ChooseScenario");
		model.addObject("userScenario", userScenario);
		return model;
	}
	
	@RequestMapping(value="/createSurvey",method=RequestMethod.POST)
	ModelAndView createSurvey(@ModelAttribute UserScenario userScenario){
		String username=userScenario.getUser().getUsername();
		User user=userService.findByUsername(username);
		Scenario scenario=userScenario.getScenario();
		userScenario.setUser(user);
		Date date=new Date();
		Timestamp createAt=new Timestamp(date.getTime());
		userScenario.setCreatedAt(createAt);
		List<UserScenarioQuestion> listUserScenarioQuestion=new ArrayList<>();
		userScenario.setUserScenarioQuestions(listUserScenarioQuestion);
		userScenarioService.save(userScenario);
		
		userScenario = user.getUserScenarios().get(user.getUserScenarios().size()-1);
	
		List<ScenarioCategory> listScenarioCategoy=scenario.getScenarioCategories();
		SurveyVO surveyVO=new SurveyVO();
		surveyVO.setUserScenarioId(userScenario.getUserScenarioId());
		List<QuestionVO> listQuestionVO=new ArrayList<>();
		for(ScenarioCategory scenarioCategory:listScenarioCategoy) {
			Category category=scenarioCategory.getCategory();
			int numQuestion=scenarioCategory.getNumquestion();
			List<Question> listQuestion=category.getQuestions();
			Collections.shuffle(listQuestion);
			for(int i=0;i<numQuestion;i++) {
				QuestionVO questionVO=new QuestionVO();
				questionVO.setQuestionId(listQuestion.get(i).getQuestionId());
				questionVO.setQuestionContent(listQuestion.get(i).getQuestionContent());
				listQuestionVO.add(questionVO);
			}
			surveyVO.setListQuestionVO(listQuestionVO);
		}
		ModelAndView modelAndView=new ModelAndView("Survey");
		modelAndView.addObject("surveyVO", surveyVO);
		modelAndView.addObject("userScenario", userScenario);
		return modelAndView;
	}	
	@RequestMapping(value="/result",method=RequestMethod.POST)
	ModelAndView result(@ModelAttribute SurveyVO surveyVO){
		UserScenario userScenario=userScenarioService.findOne(surveyVO.getUserScenarioId());
		for(QuestionVO questionVO:surveyVO.getListQuestionVO()) {
			Question question=questionService.findOne(questionVO.getQuestionId());
			UserScenarioQuestion userScenarioQuestion=new UserScenarioQuestion();
			userScenarioQuestion.setUserScenario(userScenario);
			userScenarioQuestion.setQuestion(question);
			userScenarioQuestion.setAnswer(questionVO.getAnswer());
			userScenarioQuestionService.save(userScenarioQuestion);
		}
		ModelAndView model=new ModelAndView("redirect:/listResult");
		return model;
	}
	@RequestMapping(value="/detailResult",method=RequestMethod.GET)
	ModelAndView detailResult(HttpServletRequest request){
		int userScenarioId=Integer.parseInt(request.getParameter("userScenarioId"));
		UserScenario userScenario=userScenarioService.findOne(userScenarioId);
		Scenario scenario=userScenario.getScenario();
		ResultVO resultVO=new ResultVO();
		resultVO.setUserScenarioId(userScenarioId);
		userScenario.setUser(userScenario.getUser());
		
		int totalScore=0;
		List<GroupaVO> listGroupaVO=new ArrayList<>();
		for(ScenarioGroupa scenarioGroupa:scenario.getScenarioGroupas()) {
			GroupaVO groupaVO=new GroupaVO();
			groupaVO.setGroupaId(scenarioGroupa.getGroupa().getGroupaId());
			
			groupaVO.setGroupaName(scenarioGroupa.getGroupa().getGroupaName());
			groupaVO.setWeight(scenarioGroupa.getWeight());
			List<AttributeVO> listAttributeVO=new ArrayList<>();
			//System.out.println(scenarioGroupa.getScenarioGroupaAttributes());
			int scoreGroupa=0;
			for(ScenarioGroupaAttribute scenarioGroupaAttribute:scenarioGroupa.getScenarioGroupaAttributes()) {
				Attribute attribute=scenarioGroupaAttribute.getAttribute();
				//System.out.println(attribute.getAttributeId());
				//System.out.println(userScenario.getUserScenarioId());
				AttributeVO attributeVO=questionAttributeService.AttributeVOInSurVey(attribute, userScenario);
				attributeVO.setScore(scenarioGroupaAttribute.getScore());
				listAttributeVO.add(attributeVO);
				scoreGroupa=scoreGroupa+attributeVO.getScore()*attributeVO.getScoreAttribute()/100;
			}
			groupaVO.setListAttributeVO(listAttributeVO);
			groupaVO.setScoreGroupa(scoreGroupa);
			listGroupaVO.add(groupaVO);
			totalScore=totalScore+ groupaVO.getScoreGroupa()*groupaVO.getWeight();
		}
		resultVO.setListGroupaVO(listGroupaVO);
		resultVO.setTotalScore(totalScore);
		ModelAndView modelAndView= new ModelAndView("Result");
		modelAndView.addObject("resultVO",resultVO);
		return modelAndView;
	}
}
