package com.survey.controller;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.Attribute;
import com.survey.domain.Category;
import com.survey.domain.Groupa;
import com.survey.domain.Scenario;
import com.survey.domain.ScenarioCategory;
import com.survey.domain.ScenarioGroupa;
import com.survey.domain.ScenarioGroupaAttribute;
import com.survey.service.AttributeService;
import com.survey.service.CategoryService;
import com.survey.service.GroupaService;
import com.survey.service.ScenarioCategoryService;
import com.survey.service.ScenarioGroupaAttributeService;
import com.survey.service.ScenarioGroupaService;
import com.survey.service.ScenarioService;
import com.survey.web.entities.vo.AttributeVO;
import com.survey.web.entities.vo.CategoryVO;
import com.survey.web.entities.vo.GroupaVO;
import com.survey.web.entities.vo.ScenarioVO;

@Controller
public class ScenarioController {
	public static int i=0;
	@Autowired
	public ScenarioCategoryService scenarioCategoryService;
	
	@Autowired
	ScenarioService scenarioService;
	
	@Autowired
	public CategoryService categoryService;
	
	@Autowired
	public AttributeService attributeService;
	
	@Autowired 
	public GroupaService groupaService;
	
	@Autowired
	public ScenarioGroupaService scenarioGroupaService;
	
	@Autowired
	public ScenarioGroupaAttributeService scenarioGroupaAttributeService;
	
	
	@ModelAttribute("allCategoryVO")
	public List<CategoryVO> populateVarieties() {
		List<Category> listCategory=(List<Category>) this.categoryService.findAll();
		List<CategoryVO> listCategoryVO=new ArrayList<>();
		for(Category category:listCategory){
			CategoryVO categoryVO=new CategoryVO();
			categoryVO.setCategoryId(category.getCategoryId());
			categoryVO.setCategoryName(category.getCategoryName());
			categoryVO.setNumQuestion(0);
			categoryVO.setChecked(false);
			listCategoryVO.add(categoryVO);
		}
	    return (List<CategoryVO>) listCategoryVO;
	}
	
	@ModelAttribute("allAttribute")
	public List<Attribute> allAttributeVO() {
	    return (List<Attribute>)attributeService.findAll();
	}
	
	@RequestMapping(value="/admin/listScenario",method=RequestMethod.GET)
	String listScenario(Model model){
		model.addAttribute("scenarios", scenarioService.findAll());
		return "listScenario";
	}
	@RequestMapping("/admin/editScenario")
	ModelAndView editScenario(HttpServletRequest request){
		int scenarioId=Integer.parseInt(request.getParameter("scenarioId"));
		Scenario scenario=scenarioService.findOne(scenarioId);
		ModelAndView modelAndView=new ModelAndView("ScenarioForm");
		modelAndView.addObject("scenario",scenario);
		return modelAndView;
	}
	@RequestMapping(value="/admin/deleteScenario",method=RequestMethod.GET)
	ModelAndView deleteScenario(HttpServletRequest request){
		int scenarioId=Integer.parseInt(request.getParameter("scenarioId"));
		scenarioService.delete(scenarioId);
		return new ModelAndView("redirect:/admin/listScenario");
	}
	@RequestMapping(value="/admin/addScenario",method=RequestMethod.GET)
	ModelAndView addScenario(ModelAndView model){
		Scenario scenario=new Scenario();
		model.addObject(scenario);
		model.setViewName("ScenarioForm");
		return model;
	}
	@RequestMapping(value="/admin/saveScenario",method=RequestMethod.POST)
	public ModelAndView saveScenario(@ModelAttribute Scenario scenario){
		scenarioService.save(scenario);
		return new ModelAndView("redirect:/admin/listScenario");
	}
	@RequestMapping(value="/admin/scenarioDetail", method = RequestMethod.GET)
	public ModelAndView scenarioDetail(HttpServletRequest request){
		int scenarioId=Integer.parseInt(request.getParameter("scenarioId"));
		Scenario scenario=scenarioService.findOne(scenarioId);
		ModelAndView model=new ModelAndView("ScenarioDetail");
		model.addObject("scenario", scenario);
		return model;
	}
	
	@RequestMapping(value = "/admin/scenarioSetting1", method = RequestMethod.GET)
	public ModelAndView scenarioCategory(HttpServletRequest request) {
		int scenarioId=Integer.parseInt(request.getParameter("scenarioId"));
		Scenario scenario=scenarioService.findOne(scenarioId);
		ScenarioVO scenarioVO=new ScenarioVO(scenarioId, scenario.getScenarioName());
		List<CategoryVO> listCategoryVO=this.populateVarieties();
//		for(CategoryVO categoryVO:scenarioVO.getListCategoryVO()){
//			Category category=categoryService.findOne(categoryVO.getCategoryId());
//			if(scenario.getScenarioCategories().contains(category)){
//				categoryVO.setChecked(true);
//			}
//		}
		scenarioVO.setListCategoryVO(listCategoryVO);
		List<Integer> listAttributeId=new ArrayList<>();
		List<GroupaVO> listGroupaVO=new ArrayList<>();
//		List<AttributeVO> ListAttributeVO=new ArrayList<>();
//		for(ScenarioGroupa scenarioGroupa:scenario.getScenarioGroupas()){
//			GroupaVO groupaVO=new GroupaVO();
//			groupaVO.setGroupaId(scenarioGroupa.getGroupa().getGroupaId());
//			groupaVO.setGroupaName(scenarioGroupa.getGroupa().getGroupaName());
//			groupaVO.setWeight(scenarioGroupa.getWeight());
//			
//			for(ScenarioGroupaAttribute scenarioGroupaAttribute:scenarioGroupa.getScenarioGroupaAttributes()){
//				AttributeVO attributeVO=new AttributeVO();
//				attributeVO.setAttributeId(scenarioGroupaAttribute.getAttribute().getAttributeId());
//				attributeVO.setAttributeName(scenarioGroupaAttribute.getAttribute().getAttributeName());
//				attributeVO.setAttributeScore(scenarioGroupaAttribute.getScore());
//				attributeVO.set
//			}
//		}
		
		scenarioVO.setListGroupaVO(listGroupaVO);	
		scenarioVO.setListAttributeId(listAttributeId);
		ModelAndView model = new ModelAndView("ScenarioSetting1");
		model.addObject("scenarioVO", scenarioVO);
		return model;
	}
	@RequestMapping(value="/admin/scenarioSetting1",params={"addGroupa"})
	public ModelAndView addGroupa(final ScenarioVO scenarioVO,final BindingResult bindingResult){
		GroupaVO groupaVO=new GroupaVO();
		groupaVO.setGroupaId(i++);
		//System.out.println(groupaVO.getGroupaId());
		scenarioVO.getListGroupaVO().add(groupaVO);
		ModelAndView model=new ModelAndView("ScenarioSetting1");
		model.addObject("scenarioVO", scenarioVO);
		return model;
	}
	@RequestMapping(value="/admin/scenarioSetting1",params={"remoteGroupa"})
	public ModelAndView deleteGroupa(final ScenarioVO scenarioVO,final BindingResult bindingResult,final HttpServletRequest req){	
		final Integer groupaId=Integer.valueOf(req.getParameter("remoteGroupa"));
		scenarioVO.getListGroupaVO().remove(groupaId.intValue());
		ModelAndView model=new ModelAndView("ScenarioSetting1");
		model.addObject("scenarioVO", scenarioVO);
		return model;
	}
	@RequestMapping(value="/admin/scenarioSetting1",method=RequestMethod.POST)
	public ModelAndView scenarioSetting1(final ScenarioVO scenarioVO,final BindingResult bindingResult){
		//System.out.println(scenarioVO.getListCategoryVO().size());
		//System.out.println(scenarioVO.getListAttributeVO().size());
		//System.out.println(scenarioVO.getListGroupaVO());
		if(scenarioVO.getListAttributeVO().size()==0){
			scenarioVO.setListAttributeVO(new ArrayList<>());
		}
		for(int attributeId:scenarioVO.getListAttributeId()){
			Boolean exist=false;
			Attribute attribute=attributeService.findOne(attributeId);
			for(AttributeVO attributeVO:scenarioVO.getListAttributeVO()){
				if(attributeVO.getAttributeId()==attribute.getAttributeId()){
					exist=true;
					break;
				}
			}
			if(exist==false){
				AttributeVO attributeVO=new AttributeVO(attribute);
				scenarioVO.getListAttributeVO().add(attributeVO);
			}
			exist=false;
		}
		ModelAndView model=new ModelAndView("ScenarioSetting2");
		model.addObject("scenarioVO",scenarioVO);
		return model;
	}

	
	@RequestMapping(value="/admin/scenarioSetting2",params={"previous"})
	public ModelAndView previousModel(final ScenarioVO scenarioVO,final BindingResult bindingResult){
			//System.out.println(scenarioVO.getListAttributeId().size());
			for(AttributeVO attributeVO:scenarioVO.getListAttributeVO()){
				for(GroupaVO groupaVO:scenarioVO.getListGroupaVO()){
					if(groupaVO.getGroupaId()==attributeVO.getGroupaVO().getGroupaId()){
						attributeVO.setGroupaVO(groupaVO);
					}
				}
			}
			ModelAndView model=new ModelAndView("ScenarioSetting1");
			model.addObject("scenarioVO",scenarioVO);
			return model;
	}
	@RequestMapping(value="/admin/scenarioSetting2",method=RequestMethod.POST)
	public ModelAndView scenarioSetting2(final ScenarioVO scenarioVO,final BindingResult bindingResult){
		Scenario scenario=scenarioService.findOne(scenarioVO.getScenarioId());
		for(ScenarioCategory scenarioCategory:scenario.getScenarioCategories()){
			scenarioCategoryService.delete(scenarioCategory);
		}
		for(ScenarioGroupa scenarioGroupa:scenario.getScenarioGroupas()){
			for(ScenarioGroupaAttribute scenarioGroupaAttribute:scenarioGroupa.getScenarioGroupaAttributes()){
				scenarioGroupaAttributeService.delete(scenarioGroupaAttribute);
			}
			scenarioGroupaService.delete(scenarioGroupa);
		}
		scenario.setScenarioCategories(new ArrayList<>());
		scenario.setScenarioGroupas(new ArrayList<>());
		
		
		// set attributeVO for groupaVO
		for(GroupaVO groupaVO:scenarioVO.getListGroupaVO()){
			List<AttributeVO> listAttributeVO=new ArrayList<>();
			for(AttributeVO attributeVO:scenarioVO.getListAttributeVO()){
				if(attributeVO.getGroupaVO().getGroupaId()==groupaVO.getGroupaId()){
					listAttributeVO.add(attributeVO);
				}
			}
			groupaVO.setListAttributeVO(listAttributeVO);
		}
		
		//add category
		for(CategoryVO categoryVO:scenarioVO.getListCategoryVO()){
			if(categoryVO.isChecked()==true){
			Category category=categoryService.findOne(categoryVO.getCategoryId());
			ScenarioCategory scenarioCategory= scenarioCategoryService.findByScenarioAndCategory(scenario, category);
			if(scenarioCategory==null){
				scenarioCategory=new ScenarioCategory();
				scenarioCategory.setScenario(scenario);
				scenarioCategory.setCategory(category);
			}
			scenarioCategory.setNumquestion(categoryVO.getNumQuestion());
			scenarioCategoryService.save(scenarioCategory);
			scenario.addScenarioCategory(scenarioCategory);
			}
			}
		
		//add Groupa
		for(GroupaVO groupaVO:scenarioVO.getListGroupaVO()){
			Groupa groupa=groupaService.findByGroupaName(groupaVO.getGroupaName());
			if(groupa==null){
				groupa=new Groupa();
				groupa.setGroupaName(groupaVO.getGroupaName());
			}
			groupaService.save(groupa);
			ScenarioGroupa scenarioGroupa=scenarioGroupaService.findByScenarioAndGroupa(scenario,groupa);
			if(scenarioGroupa==null){
				scenarioGroupa=new ScenarioGroupa();
				scenarioGroupa.setGroupa(groupa);
				scenarioGroupa.setScenario(scenario);
				scenarioGroupa.setWeight(groupaVO.getWeight());
				scenarioGroupa.setScenarioGroupaAttributes(new ArrayList<>());
			}
			scenarioGroupaService.save(scenarioGroupa);
			scenario.addScenarioGroupa(scenarioGroupa);
			for(AttributeVO attributeVO: groupaVO.getListAttributeVO()){
				Attribute attribute=attributeService.findOne(attributeVO.getAttributeId());
				ScenarioGroupaAttribute scenarioGroupaAttribute=scenarioGroupaAttributeService.findByScenarioGroupaAndAttribute(scenarioGroupa,attribute);
				if(scenarioGroupaAttribute==null){
					scenarioGroupaAttribute=new ScenarioGroupaAttribute();
					scenarioGroupaAttribute.setScenarioGroupa(scenarioGroupa);
					scenarioGroupaAttribute.setAttribute(attribute);
				}
				scenarioGroupaAttribute.setScore(attributeVO.getScore());
				scenarioGroupaAttributeService.save(scenarioGroupaAttribute);
				scenarioGroupa.addScenarioGroupaAttribute(scenarioGroupaAttribute);
			}
			
		}
		ModelAndView model=new ModelAndView("ScenarioDetail");
		model.addObject("scenario",scenario);
		return model;
	}
	
//	@RequestMapping(value="/admin/saveScenarioCategory",method=RequestMethod.POST)
//	public ModelAndView ScenarioCategory(@ModelAttribute ScenarioVO scenarioVO){
//		//System.out.println(scenarioVO.getListCategoryVO().size() );
//		Scenario scenario=scenarioService.findOne(scenarioVO.getScenarioId());
//		for(CategoryVO categoryVO:scenarioVO.getListCategoryVO()){
//			if(categoryVO.isChecked()){
//				Category category=categoryService.findOne(categoryVO.getCategoryId());
//				ScenarioCategory scenarioCategory=scenarioCategoryService.findByScenarioAndCategory(scenario,category);
//				if(scenarioCategory==null){
//					System.out.println(scenario.getScenarioName()+" "+category.getCategoryName());
//					scenarioCategory=new ScenarioCategory();
//				}
//				scenarioCategory.setScenario(scenario);
//				scenarioCategory.setCategory(category);
//				scenarioCategory.setNumquestion(categoryVO.getNumQuestion());
//				scenarioCategoryService.save(scenarioCategory);
//				for(AttributeVO attributeVO:categoryVO.getListAttributeVO()){
//					Attribute attribute=attributeService.findOne(attributeVO.getAttributeId());
//					//System.out.println(attribute.getAttributeId());
//					ScenarioCategoryAttribute scenarioCategoryAttribute= scenarioCategoryAttributeService.findByScenarioCategoryAndAttribute(scenarioCategory,attribute);
//					if(scenarioCategoryAttribute==null)
//						scenarioCategoryAttribute=new ScenarioCategoryAttribute();
//					scenarioCategoryAttribute.setAttribute(attribute);
//					scenarioCategoryAttribute.setScenarioCategory(scenarioCategory);
//					scenarioCategoryAttribute.setScore(attributeVO.getScore());
//					scenarioCategoryAttributeService.save(scenarioCategoryAttribute);
//				}
//			}
//			}
//		return new ModelAndView("redirect:/admin/listScenario");
//	}
	
}
