package com.survey.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.survey.domain.Category;
import com.survey.service.CategoryService;

@Controller
public class CategoryController {
	@Autowired
	public CategoryService categoryService;
	
	@RequestMapping(value="/admin/listCategory",method=RequestMethod.GET)
	String listCategory(Model model){
		model.addAttribute("categories",categoryService.findAll());
		return "ListCategory";
	}
	
	@RequestMapping("/admin/editCategory")
	ModelAndView editCatgory(HttpServletRequest request){
		int categoryId=Integer.parseInt(request.getParameter("categoryId"));
		Category category=categoryService.findOne(categoryId);
		ModelAndView modelAndView=new ModelAndView("CategoryForm");
		modelAndView.addObject("category",category);
		return modelAndView;
	}
	@RequestMapping(value="/admin/deleteCategory",method=RequestMethod.GET)
	ModelAndView deleteCategory(HttpServletRequest request){
		int categoryId=Integer.parseInt(request.getParameter("categoryId"));
		categoryService.delete(categoryId);
		return new ModelAndView("redirect:/admin/listCategory");
	}
	@RequestMapping(value="/admin/addCategory",method=RequestMethod.GET)
	ModelAndView addCategory(ModelAndView model){
		Category category=new Category();
		model.addObject(category);
		model.setViewName("CategoryForm");
		return model;
	}
	@RequestMapping(value="/admin/saveCategory",method=RequestMethod.POST)
	public ModelAndView saveCategory(@ModelAttribute Category category){
		categoryService.save(category);
		return new ModelAndView("redirect:/admin/listCategory");
	}
	
}
