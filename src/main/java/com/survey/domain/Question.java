package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the question database table.
 * 
 */
@Entity
@NamedQuery(name="Question.findAll", query="SELECT q FROM Question q")
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="question_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int questionId;

	@Column(name="question_content")
	private String questionContent;

	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="category_id")
	private Category category;

	//bi-directional many-to-one association to QuestionAttribute
	@OneToMany(mappedBy="question")
	private List<QuestionAttribute> questionAttributes;

	//bi-directional many-to-one association to UserScenarioQuestion
	@OneToMany(mappedBy="question")
	private List<UserScenarioQuestion> userScenarioQuestions;

	public Question() {
	}

	public int getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	public String getQuestionContent() {
		return this.questionContent;
	}

	

	public Category getCategory() {
		return category;
	}

	public void setQuestionContent(String questionContent) {
		this.questionContent = questionContent;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public List<QuestionAttribute> getQuestionAttributes() {
		return this.questionAttributes;
	}

	public void setQuestionAttributes(List<QuestionAttribute> questionAttributes) {
		this.questionAttributes = questionAttributes;
	}

	public QuestionAttribute addQuestionAttribute(QuestionAttribute questionAttribute) {
		getQuestionAttributes().add(questionAttribute);
		questionAttribute.setQuestion(this);

		return questionAttribute;
	}

	public QuestionAttribute removeQuestionAttribute(QuestionAttribute questionAttribute) {
		getQuestionAttributes().remove(questionAttribute);
		questionAttribute.setQuestion(null);

		return questionAttribute;
	}

	public List<UserScenarioQuestion> getUserScenarioQuestions() {
		return this.userScenarioQuestions;
	}

	public void setUserScenarioQuestions(List<UserScenarioQuestion> userScenarioQuestions) {
		this.userScenarioQuestions = userScenarioQuestions;
	}

	public UserScenarioQuestion addUserScenarioQuestion(UserScenarioQuestion userScenarioQuestion) {
		getUserScenarioQuestions().add(userScenarioQuestion);
		userScenarioQuestion.setQuestion(this);

		return userScenarioQuestion;
	}

	public UserScenarioQuestion removeUserScenarioQuestion(UserScenarioQuestion userScenarioQuestion) {
		getUserScenarioQuestions().remove(userScenarioQuestion);
		userScenarioQuestion.setQuestion(null);

		return userScenarioQuestion;
	}

}