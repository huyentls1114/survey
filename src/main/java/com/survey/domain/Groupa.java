package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the groupa database table.
 * 
 */
@Entity
@NamedQuery(name="Groupa.findAll", query="SELECT g FROM Groupa g")
public class Groupa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="groupa_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int groupaId;

	@Column(name="groupa_name")
	private String groupaName;

	//bi-directional many-to-one association to ScenarioGroupa
	@OneToMany(mappedBy="groupa")
	private List<ScenarioGroupa> scenarioGroupas;

	public Groupa() {
	}

	public int getGroupaId() {
		return this.groupaId;
	}

	public void setGroupaId(int groupaId) {
		this.groupaId = groupaId;
	}

	public String getGroupaName() {
		return this.groupaName;
	}

	public void setGroupaName(String groupaName) {
		this.groupaName = groupaName;
	}

	public List<ScenarioGroupa> getScenarioGroupas() {
		return this.scenarioGroupas;
	}

	public void setScenarioGroupas(List<ScenarioGroupa> scenarioGroupas) {
		this.scenarioGroupas = scenarioGroupas;
	}

	public ScenarioGroupa addScenarioGroupa(ScenarioGroupa scenarioGroupa) {
		getScenarioGroupas().add(scenarioGroupa);
		scenarioGroupa.setGroupa(this);

		return scenarioGroupa;
	}

	public ScenarioGroupa removeScenarioGroupa(ScenarioGroupa scenarioGroupa) {
		getScenarioGroupas().remove(scenarioGroupa);
		scenarioGroupa.setGroupa(null);

		return scenarioGroupa;
	}

}