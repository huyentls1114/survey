package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the scenario database table.
 * 
 */
@Entity
@NamedQuery(name="Scenario.findAll", query="SELECT s FROM Scenario s")
public class Scenario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="scenario_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int scenarioId;

	@Column(name="scenario_name")
	private String scenarioName;

	//bi-directional many-to-one association to ScenarioCategory
	@OneToMany(mappedBy="scenario")
	private List<ScenarioCategory> scenarioCategories;

	//bi-directional many-to-one association to UserScenario
	@OneToMany(mappedBy="scenario")
	private List<UserScenario> userScenarios;

	//bi-directional many-to-one association to ScenarioGroupa
	@OneToMany(mappedBy="scenario")
	private List<ScenarioGroupa> scenarioGroupas;

	public Scenario() {
	}

	public int getScenarioId() {
		return this.scenarioId;
	}

	public void setScenarioId(int scenarioId) {
		this.scenarioId = scenarioId;
	}

	public String getScenarioName() {
		return this.scenarioName;
	}

	public void setScenarioName(String scenarioName) {
		this.scenarioName = scenarioName;
	}

	public List<ScenarioCategory> getScenarioCategories() {
		return this.scenarioCategories;
	}

	public void setScenarioCategories(List<ScenarioCategory> scenarioCategories) {
		this.scenarioCategories = scenarioCategories;
	}

	public ScenarioCategory addScenarioCategory(ScenarioCategory scenarioCategory) {
		getScenarioCategories().add(scenarioCategory);
		scenarioCategory.setScenario(this);

		return scenarioCategory;
	}

	public ScenarioCategory removeScenarioCategory(ScenarioCategory scenarioCategory) {
		getScenarioCategories().remove(scenarioCategory);
		scenarioCategory.setScenario(null);

		return scenarioCategory;
	}

	public List<UserScenario> getUserScenarios() {
		return this.userScenarios;
	}

	public void setUserScenarios(List<UserScenario> userScenarios) {
		this.userScenarios = userScenarios;
	}

	public UserScenario addUserScenario(UserScenario userScenario) {
		getUserScenarios().add(userScenario);
		userScenario.setScenario(this);

		return userScenario;
	}

	public UserScenario removeUserScenario(UserScenario userScenario) {
		getUserScenarios().remove(userScenario);
		userScenario.setScenario(null);

		return userScenario;
	}

	public List<ScenarioGroupa> getScenarioGroupas() {
		return this.scenarioGroupas;
	}

	public void setScenarioGroupas(List<ScenarioGroupa> scenarioGroupas) {
		this.scenarioGroupas = scenarioGroupas;
	}

	public ScenarioGroupa addScenarioGroupa(ScenarioGroupa scenarioGroupa) {
		getScenarioGroupas().add(scenarioGroupa);
		scenarioGroupa.setScenario(this);

		return scenarioGroupa;
	}

	public ScenarioGroupa removeScenarioGroupa(ScenarioGroupa scenarioGroupa) {
		getScenarioGroupas().remove(scenarioGroupa);
		scenarioGroupa.setScenario(null);

		return scenarioGroupa;
	}

}