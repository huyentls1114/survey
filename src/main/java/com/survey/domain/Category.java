package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the category database table.
 * 
 */
@Entity
@NamedQuery(name="Category.findAll", query="SELECT c FROM Category c")
public class Category implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="category_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int categoryId;

	@Column(name="category_name")
	private String categoryName;

	//bi-directional many-to-one association to Question
	@OneToMany(mappedBy="category")
	private List<Question> questions;

	//bi-directional many-to-one association to ScenarioCategory
	@OneToMany(mappedBy="category")
	private List<ScenarioCategory> scenarioCategories;

	public Category() {
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<Question> getQuestions() {
		return this.questions;
	}

	public void setQuestions(List<Question> questions) {
		this.questions = questions;
	}

	public Question addQuestion(Question question) {
		getQuestions().add(question);
		question.setCategory(this);

		return question;
	}

	public Question removeQuestion(Question question) {
		getQuestions().remove(question);
		question.setCategory(null);

		return question;
	}

	public List<ScenarioCategory> getScenarioCategories() {
		return this.scenarioCategories;
	}

	public void setScenarioCategories(List<ScenarioCategory> scenarioCategories) {
		this.scenarioCategories = scenarioCategories;
	}

	public ScenarioCategory addScenarioCategory(ScenarioCategory scenarioCategory) {
		getScenarioCategories().add(scenarioCategory);
		scenarioCategory.setCategory(this);

		return scenarioCategory;
	}

	public ScenarioCategory removeScenarioCategory(ScenarioCategory scenarioCategory) {
		getScenarioCategories().remove(scenarioCategory);
		scenarioCategory.setCategory(null);

		return scenarioCategory;
	}

}