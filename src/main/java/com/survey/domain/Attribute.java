package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the attribute database table.
 * 
 */
@Entity
@NamedQuery(name="Attribute.findAll", query="SELECT a FROM Attribute a")
public class Attribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="attribute_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int attributeId;

	@Column(name="attribute_name")
	private String attributeName;

	//bi-directional many-to-one association to QuestionAttribute
	@OneToMany(mappedBy="attribute")
	private List<QuestionAttribute> questionAttributes;

	//bi-directional many-to-one association to ScenarioGroupaAttribute
	@OneToMany(mappedBy="attribute")
	private List<ScenarioGroupaAttribute> scenarioGroupaAttributes;

	public Attribute() {
	}

	public int getAttributeId() {
		return this.attributeId;
	}

	public void setAttributeId(int attributeId) {
		this.attributeId = attributeId;
	}

	public String getAttributeName() {
		return this.attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public List<QuestionAttribute> getQuestionAttributes() {
		return this.questionAttributes;
	}

	public void setQuestionAttributes(List<QuestionAttribute> questionAttributes) {
		this.questionAttributes = questionAttributes;
	}

	public QuestionAttribute addQuestionAttribute(QuestionAttribute questionAttribute) {
		getQuestionAttributes().add(questionAttribute);
		questionAttribute.setAttribute(this);

		return questionAttribute;
	}

	public QuestionAttribute removeQuestionAttribute(QuestionAttribute questionAttribute) {
		getQuestionAttributes().remove(questionAttribute);
		questionAttribute.setAttribute(null);

		return questionAttribute;
	}

	public List<ScenarioGroupaAttribute> getScenarioGroupaAttributes() {
		return this.scenarioGroupaAttributes;
	}

	public void setScenarioGroupaAttributes(List<ScenarioGroupaAttribute> scenarioGroupaAttributes) {
		this.scenarioGroupaAttributes = scenarioGroupaAttributes;
	}

	public ScenarioGroupaAttribute addScenarioGroupaAttribute(ScenarioGroupaAttribute scenarioGroupaAttribute) {
		getScenarioGroupaAttributes().add(scenarioGroupaAttribute);
		scenarioGroupaAttribute.setAttribute(this);

		return scenarioGroupaAttribute;
	}

	public ScenarioGroupaAttribute removeScenarioGroupaAttribute(ScenarioGroupaAttribute scenarioGroupaAttribute) {
		getScenarioGroupaAttributes().remove(scenarioGroupaAttribute);
		scenarioGroupaAttribute.setAttribute(null);

		return scenarioGroupaAttribute;
	}

}