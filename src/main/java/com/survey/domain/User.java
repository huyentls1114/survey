package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;

	private String password;

	private String role;

	private String username;

	//bi-directional many-to-one association to UserScenario
	@OneToMany(mappedBy="user")
	private List<UserScenario> userScenarios;

	public User() {
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<UserScenario> getUserScenarios() {
		return this.userScenarios;
	}

	public void setUserScenarios(List<UserScenario> userScenarios) {
		this.userScenarios = userScenarios;
	}

	public UserScenario addUserScenario(UserScenario userScenario) {
		getUserScenarios().add(userScenario);
		userScenario.setUser(this);

		return userScenario;
	}

	public UserScenario removeUserScenario(UserScenario userScenario) {
		getUserScenarios().remove(userScenario);
		userScenario.setUser(null);
		return userScenario;
	}

}