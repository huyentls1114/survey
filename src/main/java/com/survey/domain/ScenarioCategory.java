package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the scenario_category database table.
 * 
 */
@Entity
@Table(name="scenario_category")
@NamedQuery(name="ScenarioCategory.findAll", query="SELECT s FROM ScenarioCategory s")
public class ScenarioCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="scencario_category_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int scencarioCategoryId;

	private int numquestion;

	//bi-directional many-to-one association to Category
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="category_id")
	private Category category;

	//bi-directional many-to-one association to Scenario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="scenario_id")
	private Scenario scenario;

	public ScenarioCategory() {
	}

	public int getScencarioCategoryId() {
		return this.scencarioCategoryId;
	}

	public void setScencarioCategoryId(int scencarioCategoryId) {
		this.scencarioCategoryId = scencarioCategoryId;
	}

	public int getNumquestion() {
		return this.numquestion;
	}

	public void setNumquestion(int numquestion) {
		this.numquestion = numquestion;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public Scenario getScenario() {
		return this.scenario;
	}

	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}

}