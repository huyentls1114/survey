package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the question_attribute database table.
 * 
 */
@Entity
@Table(name="question_attribute")
@NamedQuery(name="QuestionAttribute.findAll", query="SELECT q FROM QuestionAttribute q")
public class QuestionAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="question_attribute_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int questionAttributeId;

	//bi-directional many-to-one association to Attribute
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="attribute_id")
	private Attribute attribute;

	//bi-directional many-to-one association to Question
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="question_id")
	private Question question;

	public QuestionAttribute() {
	}

	public int getQuestionAttributeId() {
		return this.questionAttributeId;
	}

	public void setQuestionAttributeId(int questionAttributeId) {
		this.questionAttributeId = questionAttributeId;
	}

	public Attribute getAttribute() {
		return this.attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

}