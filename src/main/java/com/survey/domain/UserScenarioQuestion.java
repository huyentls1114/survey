package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_scenario_question database table.
 * 
 */
@Entity
@Table(name="user_scenario_question")
@NamedQuery(name="UserScenarioQuestion.findAll", query="SELECT u FROM UserScenarioQuestion u")
public class UserScenarioQuestion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_scenario_question_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userScenarioQuestionId;

	private boolean answer;

	//bi-directional many-to-one association to Question
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="question_id")
	private Question question;

	//bi-directional many-to-one association to UserScenario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_scenario_id")
	private UserScenario userScenario;

	public UserScenarioQuestion() {
	}

	public int getUserScenarioQuestionId() {
		return this.userScenarioQuestionId;
	}

	public void setUserScenarioQuestionId(int userScenarioQuestionId) {
		this.userScenarioQuestionId = userScenarioQuestionId;
	}

	public boolean isAnswer() {
		return answer;
	}

	public void setAnswer(boolean answer) {
		this.answer = answer;
	}

	public Question getQuestion() {
		return this.question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public UserScenario getUserScenario() {
		return this.userScenario;
	}

	public void setUserScenario(UserScenario userScenario) {
		this.userScenario = userScenario;
	}

}