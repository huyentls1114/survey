package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the scenario_groupa_attribute database table.
 * 
 */
@Entity
@Table(name="scenario_groupa_attribute")
@NamedQuery(name="ScenarioGroupaAttribute.findAll", query="SELECT s FROM ScenarioGroupaAttribute s")
public class ScenarioGroupaAttribute implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="scenario_groupa_attribute_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int scenarioGroupaAttributeId;

	private int score;

	//bi-directional many-to-one association to Attribute
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="attribute_id")
	private Attribute attribute;

	//bi-directional many-to-one association to ScenarioGroupa
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="scenario_groupa_id")
	private ScenarioGroupa scenarioGroupa;

	public ScenarioGroupaAttribute() {
	}

	public int getScenarioGroupaAttributeId() {
		return this.scenarioGroupaAttributeId;
	}

	public void setScenarioGroupaAttributeId(int scenarioGroupaAttributeId) {
		this.scenarioGroupaAttributeId = scenarioGroupaAttributeId;
	}

	public int getScore() {
		return this.score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Attribute getAttribute() {
		return this.attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public ScenarioGroupa getScenarioGroupa() {
		return this.scenarioGroupa;
	}

	public void setScenarioGroupa(ScenarioGroupa scenarioGroupa) {
		this.scenarioGroupa = scenarioGroupa;
	}

}