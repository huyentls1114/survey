package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the scenario_groupa database table.
 * 
 */
@Entity
@Table(name="scenario_groupa")
@NamedQuery(name="ScenarioGroupa.findAll", query="SELECT s FROM ScenarioGroupa s")
public class ScenarioGroupa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="scenario_groupa_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int scenarioGroupaId;

	private int weight;

	//bi-directional many-to-one association to Groupa
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="groupa_id")
	private Groupa groupa;

	//bi-directional many-to-one association to Scenario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="scenario_id")
	private Scenario scenario;

	//bi-directional many-to-one association to ScenarioGroupaAttribute
	@OneToMany(mappedBy="scenarioGroupa")
	private List<ScenarioGroupaAttribute> scenarioGroupaAttributes;

	public ScenarioGroupa() {
	}

	public int getScenarioGroupaId() {
		return this.scenarioGroupaId;
	}

	public void setScenarioGroupaId(int scenarioGroupaId) {
		this.scenarioGroupaId = scenarioGroupaId;
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public Groupa getGroupa() {
		return this.groupa;
	}

	public void setGroupa(Groupa groupa) {
		this.groupa = groupa;
	}

	public Scenario getScenario() {
		return this.scenario;
	}

	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}

	public List<ScenarioGroupaAttribute> getScenarioGroupaAttributes() {
		return this.scenarioGroupaAttributes;
	}

	public void setScenarioGroupaAttributes(List<ScenarioGroupaAttribute> scenarioGroupaAttributes) {
		this.scenarioGroupaAttributes = scenarioGroupaAttributes;
	}

	public ScenarioGroupaAttribute addScenarioGroupaAttribute(ScenarioGroupaAttribute scenarioGroupaAttribute) {
		getScenarioGroupaAttributes().add(scenarioGroupaAttribute);
		scenarioGroupaAttribute.setScenarioGroupa(this);

		return scenarioGroupaAttribute;
	}

	public ScenarioGroupaAttribute removeScenarioGroupaAttribute(ScenarioGroupaAttribute scenarioGroupaAttribute) {
		getScenarioGroupaAttributes().remove(scenarioGroupaAttribute);
		scenarioGroupaAttribute.setScenarioGroupa(null);

		return scenarioGroupaAttribute;
	}

}