package com.survey.domain;

import java.io.Serializable;
import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the user_scenario database table.
 * 
 */
@Entity
@Table(name="user_scenario")
@NamedQuery(name="UserScenario.findAll", query="SELECT u FROM UserScenario u")
public class UserScenario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="user_scenario_id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userScenarioId;

	@Column(name="created_at")
	private Timestamp createdAt;

	//bi-directional many-to-one association to Scenario
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="scenario_id")
	private Scenario scenario;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id")
	private User user;

	//bi-directional many-to-one association to UserScenarioQuestion
	@OneToMany(mappedBy="userScenario")
	private List<UserScenarioQuestion> userScenarioQuestions;

	public UserScenario() {
	}

	public int getUserScenarioId() {
		return this.userScenarioId;
	}

	public void setUserScenarioId(int userScenarioId) {
		this.userScenarioId = userScenarioId;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Scenario getScenario() {
		return this.scenario;
	}

	public void setScenario(Scenario scenario) {
		this.scenario = scenario;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<UserScenarioQuestion> getUserScenarioQuestions() {
		return this.userScenarioQuestions;
	}

	public void setUserScenarioQuestions(List<UserScenarioQuestion> userScenarioQuestions) {
		this.userScenarioQuestions = userScenarioQuestions;
	}

	public UserScenarioQuestion addUserScenarioQuestion(UserScenarioQuestion userScenarioQuestion) {
		getUserScenarioQuestions().add(userScenarioQuestion);
		userScenarioQuestion.setUserScenario(this);

		return userScenarioQuestion;
	}

	public UserScenarioQuestion removeUserScenarioQuestion(UserScenarioQuestion userScenarioQuestion) {
		getUserScenarioQuestions().remove(userScenarioQuestion);
		userScenarioQuestion.setUserScenario(null);

		return userScenarioQuestion;
	}

}